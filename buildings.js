module.exports = {
    smallHouse:{
        texture:function(){
            var variants = ["house1","house5"]
            return variants[Math.floor(Math.random()*variants.length)]
        },
        rubble:"smallrubble",
        maxElevation:10,
        minElevation:0,
        maxOccupants:20,
        type:"residential"
    },
    mediumHouse:{
        texture:function(){
            var variants = ["house6","house2","house3","house4"]
            return variants[Math.floor(Math.random()*variants.length)]
        },
        rubble:"mediumrubble",
        maxElevation:7,
        minElevation:3,
        maxOccupants:50,
        type:"residential"
    },
    forestry:{
        texture:function(){
            return "forestry1";
        },
        rubble:"mediumrubble",
        maxElevation:7,
        minElevation:0,
        maxOccupants:5,
        type:"industrial",
        output:"wood",
        input:null
    },

    mine:{
        texture:function(){
            return "mine1";
        },
        rubble:"mediumrubble",
        maxElevation:20,
        minElevation:5,
        maxOccupants:20,
        type:"industrial",
        output:"ore",
        input:null
    },
    forge:{
        texture:function(){
            return "forge1";
        },
        rubble:"mediumrubble",
        maxElevation:7,
        minElevation:2,
        maxOccupants:20,
        type:"industrial",
        output:"metal",
        input:"ore"
    }
}