

var val = "";
var port = 3979;
var userTotal = 0;
var config = {
	width:30,
	height:30,
	sealevel:55,
	startpop:50
}
var grid = []
var explosioncooldown = 0;
var express = require('express');
var app     = express();
//var server  = app.listen(port);
var server      = app.listen(port);
var io = require('socket.io').listen(server,{pingTimeout: 30000, pingInterval: 100});
io.set('heartbeat timeout', 30000);
io.set('heartbeat interval', 4);


class tile{
	constructor(x,y){
		this.x = x;
		this.y = y;
		this.biome = "forest";
		this.water = false;
		this.building = null;
		this.elevation = 0;
		this.forestDepth = 0;
		this.texture = null;
        this.type = null;
        this.owner = null;
		this.changed = false;
		this.rubble = false;
		this.maxOccupants = 0;
		this.occupants = 0;
	}

	update(){
		//TODO
	}
}

function dist(ax,ay,bx,by){
	return Math.sqrt(((ax-bx)*(ax-bx))+((ay-by)*(ay-by)));
}

function oob(x,y){
	if(x < 0 || y < 0 || y > config.height-1 || x > config.width-1){
		return true;
	}else{
		return false;
	}
}

var smoothness = 5;

for(var x = 0; x < config.width; x++){
    grid[x] = [];
    for(var y = 0; y < config.height; y++){
        grid[x][y] = new tile(x,y);
        grid[x][y].elevation = Math.round(Math.random()*100)+5;
    }
}

for(var k = 0; k < smoothness; k++){
    for(var x = 0; x < config.width; x++){
        for(var y = 0; y < config.height; y++){
            var avg = 0;
            var count = 0;
            for(var i = -1; i < 2; i++){
                for(var j = -1; j < 2; j++){
                    if(!oob(x+i,y+j)){
                        count++;
                        avg+=grid[x+i][y+j].elevation;
                    }
                }
            }
            grid[x][y].elevation = (avg/count)+((Math.random()*1)-0.5)
        }
    }
}

for(var x = 0; x < config.width; x++){
    for(var y = 0; y < config.height; y++){
        if(grid[x][y].elevation < config.sealevel){
            grid[x][y].water = true;
        }else{
			if(Math.random() > 0.99){
				grid[x][y].forestDepth = 1;
			}
		}

    }
}

var Vector = function(x,y) {
    this.x = x;
    this.y = y;
} 

Vector.prototype.normalize = function() {
	var length = Math.sqrt(this.x*this.x+this.y*this.y); //calculating length
	this.x = this.x/length; //assigning new value to x (dividing x by lenght of the vector)
	this.y = this.y/length;
}

for(var x = 0; x < config.width; x++){
    for(var y = 0; y < config.height; y++){
        if(grid[x][y].elevation > config.sealevel-1 && grid[x][y].elevation < config.sealevel+0.25 && Math.random() > 0.99){
            var rx = x;
            var ry = y;
            var dir = Math.random()*360;
            var steerAcceleration = 1;
            for(var i = 0; i < (Math.random()*config.width*2)+(config.width*5);i++){
                dir += (Math.random()*steerAcceleration)-(steerAcceleration/2);
                var vec = new Vector(Math.cos(dir),Math.sin(dir));
                if(steerAcceleration < 0){
                    steerAcceleration = 0;
                }else if(steerAcceleration > 2){
                    steerAcceleration = 2;
                }
                vec.normalize();
                rx += vec.x;
                ry += vec.y;

                if(oob(Math.round(rx),Math.round(ry))){
                    break;
                }
                var count = 0;
                var avg = 0;
                grid[Math.round(rx)][Math.round(ry)].elevation=config.sealevel-1;
                for(var k = 0; k < 9; k++){
                    var tx = Math.round(rx)+((k%3)-1);
                    var ty = Math.round(ry)+(Math.floor(k/3)-1);
                    if(!oob(tx,ty)){
                        for(var i = -1; i < 2; i++){
                            for(var j = -1; j < 2; j++){
                                if(!oob(tx+i,ty+j)){
                                    count++;
                                    avg+=grid[tx+i][ty+j].elevation;
                                }
                            }
                        }
                        if(grid[tx][ty].elevation > config.sealevel)
                            grid[tx][ty].elevation = (avg/count)+((Math.random()*1)-0.5)/2;
                    }
                    
                }
                grid[Math.round(rx)][Math.round(ry)].elevation=config.sealevel-1;
            }
        }

    }
}


for(var x = 0; x < config.width; x++){
    for(var y = 0; y < config.height; y++){
        if(grid[x][y].elevation < config.sealevel){
            grid[x][y].water = true;
        }

    }
}


//done
app.use(express.static("public"));

io.on('connection', function(socket){
	userTotal += 1;
	socket.emit('newVal',val);
	socket.emit("initgrid",grid);

	socket.on("getgrid",function(){
		socket.emit("initgrid",grid);
	})
});

function updateAll(){
	for(var x = 0; x < config.width; x++){
		for(var y = 0; y < config.height; y++){
			grid[x][y].update();
			if(grid[x][y].changed){
				grid[x][y].changed = false;
				io.emit("updategrid",x,y,JSON.stringify(grid[x][y]))
			}
		}
	}
	setTimeout(updateAll,tickspeed);
}

var tickspeed = 100;

function setup(){
	for(var i = 0; i < 500; i++){
		for(var x = 0; x < config.width; x++){
			for(var y = 0; y < config.height; y++){
				grid[x][y].update();
			}
		}
	}
}

setup();
updateAll();



console.log("FINISHED");

//ref();
